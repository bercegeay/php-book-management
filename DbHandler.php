<html>
   <head>
      <title>Form Validation</title>
   </head>

   <body style = "font-family: arial,sans-serif">

	<!--  Definition of Connection Variables for MySql  -->
	<?php
	$servername = "localhost";
	$username = "bercegeay";
	$password = "bercegeay341";
	$database = "w0561461";
	$table = "Library";

	extract($_POST);

	$render = false;

	#Empty Post Validation; If Executing A Search, This Does Not Apply To Allow A Search All
	if ($_SERVER["REQUEST_METHOD"] == "POST"){
		if(empty($_POST["title"]) && empty($_POST["author"]) && empty($_POST["isbn"]) && empty($_POST["publisher"]) && empty($_POST["year"]) && $options != "Search" ){
			print("Sorry, You Must Enter Some Information");
		}
		else{
			$render = true;
		}	
	}
	
	?>
	  
	<strong>
	<?php

	if($render == true){
		#Connect To The Database
		if ( !( $connection = mysql_connect( $servername, $username, $password ) ) ) {
			die( "Could not connect to database" );
		}

		#Open To The Database 
		if(!mysql_select_db( $database, $connection)){
			die("Could Not Open Database");
		}

		if($options == "Add"){
			#When Adding A Book To The Library, We Create A Query String To Be Intepreted As SQL. Therefor, We Follow The INSERT INTO Syntax With Corresponding Values.
			$query = "INSERT INTO $table (Title, Author, ISBN, Publisher, Year) VALUES ('$title', '$author', '$isbn', '$publisher', '$year')";

			#A Re-usabe Method For Querying Our Database. We Simply Change The $query String And Corresponding Messages.
			if ( !( $result = mysql_query( $query, $connection ) ) ) {
				print( "Failure: <br /><br />" );
				die( "All Book Records Must Include an ISBN" );
			}
			else{
				print("Success: The Following Book Has Been Added To The Library. ");
			}
		}
		elseif($options == "Delete"){
			#The Delete Function Is Tied To Each Book's ISBN. You Must Specify This In Order To Delete A Book Record.
			if($isbn != ''){
				print("The Following Book Has Been Removed From The Library. ");

				#First We Make A Simple Search Query To Load The Information Related To The Specified ISBN
				$query = "SELECT * FROM $table WHERE ISBN = '$isbn'";

				#Perform Our Re-Usable Query Method
				if ( !( $result = mysql_query( $query, $connection ) ) ) {
					print( "Could not execute query! <br />" );
					die( mysql_error() );
				}

				$row = mysql_fetch_row( $result ); 

				#Above We Grab The Row Of The Record With Our ISBN. Below We Assign Them To Our Local Variables Which Have Been Left Empty

				$title = $row[0];
				$author = $row[1];
				$publisher = $row[3];
				$year = $row[4];

				#Now We Create The Actual Delete Query For Said ISBN
				$query = "DELETE FROM $table WHERE ISBN = $isbn";

				if ( !( $result = mysql_query( $query, $connection ) ) ) {
					print( "Could not execute query! <br />" );
					die( mysql_error() );
				}
			}
			else{
				print("You Must Specify an ISBN");
			}

		}

		elseif($options == "Search"){
			print("Search Results: ");
		}

		elseif($options == "Edit"){

			$query = "SELECT * FROM $table WHERE ISBN = '$isbn'";

			#Perform Our Re-Usable Query Method
			if ( !( $result = mysql_query( $query, $connection ) ) ) {
				print( "Could not execute query! <br />" );
				die( mysql_error() );
			}

			$row = mysql_fetch_row( $result ); 

			#Above We Grab The Row Of The Record With Our ISBN. Below We Assign Them To Our Local Variables Which Have Been Left Empty

			

			#Editing Works From The Same Principle As Delete, You Must Specify An ISBN
			#Below Are A Series of If Statements That Decide If A Any Of The Four Sub-Queries Should Be Left Empty Or Not
			#We Are Checking Each Field; If The Field Is Empty, So To We Leave The Sub-Query Empty

			#Refer To SEARCH For More Commentation
			if($title != ''){
				$tquery = " Title = '$title' ";
			}
			else{
				$tquery = " ";
			}

			if($author != ''){
				$aquery = " Author = '$author' ";
				if($title != ''){
					$Tand = ",";
				}
			}
			else{
				$aquery = "";
				$Tand = "";
			}

			if($publisher != ''){
				$pquery = " Publisher = '$publisher' ";
				if($author != '' || $title != ''){
					$Aand = ",";
				}
			}
			else{
				$pquery = "";
				$Aand = "";
			}

			if($year != ''){
				$yquery = " Year = '$year'";
				if($publisher != '' || $author != '' || $title != ''){
					$Pand = ",";
				}
			}
			else{
				$yquery = " ";
				$Pand = "";
			}

			#These If Statements Display The Data That Was Unedited
			if($title == ''){
				$title = $row[0];
			}

			if($author == ''){
				$author = $row[1];
			}

			if($publisher == ''){
				$publisher = $row[3];
			}

			if($year == ''){
				$year = $row[4];
			}

			#Here We Construct Our Update Query And Concatenate All Sub-Queries.
			$query = "UPDATE $table SET" . $tquery . $Tand . $aquery . $Aand . $pquery . $Pand . $yquery . " WHERE ISBN = '$isbn' ";

			if ( !( $result = mysql_query( $query, $connection ) ) ) {
				print( "Failure To Update: <br /><br />" );
				die( "Could Not Locate Book Associated With ISBN: " + $isbn );
			}
			else{
				print("Success: The Following Book Has Been Updated: ");
			}
		}
	}

	?>
	</strong>
	   
	<table border = "0" cellpadding = "0" cellspacing = "10">
		<tr>
			<td bgcolor = "#ffffaa">Title </td>
			<td bgcolor = "#ffffbb">Author</td>
			<td bgcolor = "#ffffcc">ISBN</td>
			<td bgcolor = "#ffffdd">Publisher</td>
			<td bgcolor = "#ffffdd">Year</td>
		</tr>

		<tr>
		<?php
		if($options != "Search")
		{

		   // print each form field’s value
 			print( 
				"<td>$title </td>
				<td>$author</td>
				<td>$isbn</td>
				<td>$publisher</td>
				<td>$year</td>"
			);
			
		}
		?>
		</tr>
		
		<?php
		#The Search Method Works Similar To The Edit Function
		if($options == "Search")
		{
			#For Title, It Is A Simple Title Sub-Query Where We Match Title To Title
			if($title != ''){
				$tquery = " Title = '$title' ";
				
			}
			else{
				$tquery = " ";
			}

			#In This Case We Have To Introduce The AND Clause For SQL To Search By Multiple Conditions
			if($author != ''){
				$aquery = " Author = '$author' ";

				#After We Are Sure That Author Is Indeed Not Empty, We Must Now Check To See If Title Is Empty
				#If It Is Not Empty, We Must Add an AND SubQuery Between The Two
				if($title != ''){
					$Tand = "AND";
				}
			}
			else{
				$aquery = "";
				$Tand = "";
			}

			if($isbn != ''){
				$iquery = " ISBN = '$isbn' ";

				#Now We Must Scale-Up. We May Not Only Check To See If Author Is Empty, Because If The Title Field Has A Value We Need To Add An AND SubQuery
				if($author != '' || $title != ''){
					$Aand = "AND";
				}
			}
			else{
				$iquery = "";
				$Aand = "";
			}

			if($publisher != ''){
				$pquery = " Publisher = '$publisher' ";

				#The Further Down We Go, The More Checks We Must Make
				if($isbn != '' || $author != '' || $title != ''){
					$Iand = "AND";
				}
			}
			else{
				$pquery = "";
				$Iand = "";
			}

			if($year != ''){
				$yquery = " Year = '$year'";
				if($publisher != '' || $isbn != '' || $author != '' || $title != ''){
					$Pand = "AND";
				}
			}
			else{
				$yquery = "";
				$Pand = "";
			}

			#If No Fields Are Specified, We Shall Select All Records From Our Table.
			if($title == '' && $author == '' && $isbn == '' && $publisher == '' && $year == ''){
				$query = "SELECT * FROM $table";
			}
			#If Fields Are Specified Then We Will Concatenate The SubQueries
			else{
				$query = "SELECT * FROM $table WHERE" . $tquery . $Tand . $aquery . $Aand . $iquery . $Iand . $pquery . $Pand . $yquery ;
			}
			
			

			if ( !( $result = mysql_query( $query, $connection ) ) ) {
				print( "Could not execute query! <br />" );
				die( mysql_error() );
			}

			for ( 
				$counter = 0; 
				$row = mysql_fetch_row( $result ); 
				$counter++ ){

				// build table to display results
				print( "<tr>" );
			
				foreach ( $row as $key => $value ){
					print( "<td>$value</td>" );
				}

				print( "</tr>" );
			}
			
		}
		mysql_close($database);
		?>
	</table>
	<br /><br /><br />
	

	<a href="./BookManagement.html"> Back to Management </a>


   </body>
</html>
